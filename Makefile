CC=g++
CFLAGS=-Wall -g

all:

ejemplo1: ejemplo1.cpp
	$(CC) -o ejemplo1 ejemplo1.cpp

run_ejemplo1: ejemplo1
	./ejemplo1

ejemplo2: ejemplo2.cpp
	$(CC) -o $@ $^

primos: primos.cpp
	$(CC) -o $@ $^

run_ejemplo2: ejemplo2
	./ejemplo2

clean:
	rm * .o ejemplo1
