#include "primos.h"

//extracts up to max length
bool prime_div_p(int n, int list_length, int* prime_list) {
  int i = 0;
  int p = 2;
  while (n != 1 && i < list_length - 2) {
    if (n % p == 0) {
      prime_list[i++] = p;
      n = n / p;
    } else {
      p++;
    }
  }
  return ( i < list_length - 1); 
}
