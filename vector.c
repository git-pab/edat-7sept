#include <stdio.h>
#include "vector.h"

void print_vector (int* v, int len) {
  printf("[");
  if (len)
    printf("%i", v[0]);
  for (int i = 1; i < len; i++)
    printf(", %i", v[i]);
  printf("]");
}
